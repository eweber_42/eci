<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:fn="fn"
    xmlns:pl="http://product-live.com"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    version="3.0"
    exclude-result-prefixes="xs fn pl">
    <xsl:output method="xml" indent="yes" encoding="UTF-8" cdata-section-elements="" />
    <!-- CONSTANT -->
    <xsl:variable name="constantProductType">
        <attribute>
            <code>product_type</code>
            <value>
                <data>00234</data>
            </value>
            <data_type>SIMPLE_SELECT</data_type>
        </attribute>
    </xsl:variable>
    <xsl:variable name="brand">
        <attribute>
            <code>brand</code>
            <value>
                <data> MORGAN </data>
            </value>
            <data_type>SIMPLE_SELECT</data_type>
        </attribute>
    </xsl:variable>
    <!-- PARAMS -->
    <xsl:param name="paramsXLSX" select="'params_ECI.xml'" />
    <xsl:variable name="varXLSX" select="document($paramsXLSX)/Xlsx-To-Xml/Sheets" />
    <xsl:variable name="provider_ref">
        <xsl:for-each select="$varXLSX/Sheet[1]/Rows/R[4]">
            <attribute>
                <code><xsl:value-of select="C[@j = '1']"/></code>
                <value>
                    <xsl:copy-of select="$name/attribute/value/data"/>
                </value>
                <data_type><xsl:value-of select="C[@j = '3']"/></data_type>
            </attribute>
        </xsl:for-each>
    </xsl:variable>

    <xsl:variable name="tokenizedSeason">
        <xsl:value-of select="tokenize($saisonGestion, '[a-zA-Z]')"/>
    </xsl:variable>
    <xsl:variable name="code_fashion_season.year_season">
      <xsl:value-of select="concat('20', $tokenizedSeason)" />
    </xsl:variable>
    <xsl:variable name="fashion_season.year_season">
        <xsl:for-each select="$varXLSX/Sheet[1]/Rows/R[12]">
            <attribute>
                <code><xsl:value-of select="C[@j = '1']"/></code>
                <value>
                    <data>
                        <xsl:copy-of select="$code_fashion_season.year_season"/>
                    </data>
                </value>
                <data_type><xsl:value-of select="C[@j = '3']"/></data_type>
            </attribute>
        </xsl:for-each>
    </xsl:variable>

    <xsl:variable name="fashion_clothing_gender_1">
        <xsl:for-each select="$varXLSX/Sheet[1]/Rows/R[13]">
            <attribute>
                <code><xsl:value-of select="C[@j = '1']"/></code>
                <value>
                    <data>
                        <xsl:value-of select="C[@j = '5']"/>
                    </data>
                </value>
                <data_type><xsl:value-of select="C[@j = '3']"/></data_type>
            </attribute>
        </xsl:for-each>
    </xsl:variable>

    <xsl:variable name="fashion_sleeve">
        <xsl:for-each select="$varXLSX/Sheet[1]/Rows/R[29]">
            <attribute>
                <code><xsl:value-of select="C[@j = '1']"/></code>
                <value>
                    <data>
                        <xsl:value-of select="$longueur_de_manches/attribute/value/data"/>
                    </data>
                </value>
                <data_type><xsl:value-of select="C[@j = '3']"/></data_type>
            </attribute>
        </xsl:for-each>
    </xsl:variable>

    <xsl:variable name="fashion_cut_1">
        <xsl:for-each select="$varXLSX/Sheet[1]/Rows/R[30]">
            <attribute>
                <code><xsl:value-of select="C[@j = '1']"/></code>
                <value>
                    <data>
                        <xsl:value-of select="$coupe_jeans/attribute/value/data"/>
                    </data>
                </value>
                <data_type><xsl:value-of select="C[@j = '3']"/></data_type>
            </attribute>
        </xsl:for-each>
    </xsl:variable>

    <xsl:variable name="cortrarie">
        <xsl:for-each select="$varXLSX/Sheet[1]/Rows/R[3]">
            <attribute>
                <code><xsl:value-of select="C[@j = '1']"/></code>
                <value>
                    <data><xsl:value-of select="C[@j = '5']"/></data>
                </value>
                <data_type><xsl:value-of select="C[@j = '3']"/></data_type>
            </attribute>
        </xsl:for-each>
    </xsl:variable>

    <xsl:variable name="keywords">
        <xsl:for-each select="$varXLSX/Sheet[2]/Rows/R[position() > 1]">
            <attribute>
                <code><xsl:value-of select="C[@j = '1']"/></code>
                <value>
                    <!-- <data><xsl:value-of select="C[@j = '5']"/></data> -->
                </value>
                <data_type><xsl:value-of select="C[@j = '3']"/></data_type>
            </attribute>
        </xsl:for-each>
    </xsl:variable>

    <!-- Typologie -->
    <xsl:param name="paramsTY" select="'product_Typo.xml'" />
    <xsl:variable name="varTY" select="document($paramsTY)"/>



    <xsl:variable name="saisonGestion">
        <xsl:for-each select="products/product/attributes/attribute">
            <xsl:choose>
                <xsl:when test="contains(., 'SAISON_GESTION')">
                    <xsl:value-of select="value/data"/>
                </xsl:when>
                <xsl:otherwise>
                <!-- EMPTY -->
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
    </xsl:variable>

<!-- VARIABLE DECLARATION -->
    <xsl:variable name="longueur_de_manches">
        <xsl:for-each select="$varTY/products/product/attributes/attribute">
            <xsl:choose>
                <xsl:when test="contains(., 'longueur_de_manches')">
                    <attribute>
                        <code>
                            <xsl:value-of select="code"/>
                        </code>
                        <value>
                            <data>
                                <xsl:value-of select="value/data"/>
                            </data>
                            <code>
                                <xsl:value-of select="value/code"/>
                            </code>
                        </value>
                        <type>
                            <xsl:value-of select="type"/>
                        </type>
                        <data_type>
                            <xsl:value-of select="data_type"/>
                        </data_type>
                    </attribute>
                </xsl:when>
                <xsl:otherwise>
                    <!-- EMPTY -->
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
    </xsl:variable>

    <xsl:variable name="coupe_jeans">
        <xsl:for-each select="$varTY/products/product/attributes/attribute">
            <xsl:choose>
                <xsl:when test="contains(., 'coupe_jeans')">
                    <attribute>
                        <code>
                            <xsl:value-of select="code"/>
                        </code>
                        <value>
                            <data>
                                <xsl:value-of select="value/data"/>
                            </data>
                            <code>
                                <xsl:value-of select="value/code"/>
                            </code>
                        </value>
                        <type>
                            <xsl:value-of select="type"/>
                        </type>
                        <data_type>
                            <xsl:value-of select="data_type"/>
                        </data_type>
                    </attribute>
                </xsl:when>
                <xsl:otherwise>
                    <!-- EMPTY -->
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
    </xsl:variable>

    <xsl:variable name="forme_de_col">
        <xsl:for-each select="$varTY/products/product/attributes/attribute">
            <xsl:choose>
                <xsl:when test="contains(., 'forme_de_col')">
                    <attribute>
                        <code>
                            <xsl:value-of select="code"/>
                        </code>
                        <value>
                            <data>
                                <xsl:value-of select="value/data"/>
                            </data>
                            <code>
                                <xsl:value-of select="value/code"/>
                            </code>
                        </value>
                        <type>
                            <xsl:value-of select="type"/>
                        </type>
                        <data_type>
                            <xsl:value-of select="data_type"/>
                        </data_type>
                    </attribute>
                </xsl:when>
                <xsl:otherwise>
                    <!-- EMPTY -->
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
    </xsl:variable>

    <xsl:variable name="Motif_PAP">
        <xsl:for-each select="$varTY/products/product/attributes/attribute">
            <xsl:choose>
                <xsl:when test="contains(., 'length_type')">
                    <attribute>
                        <code>
                            <xsl:value-of select="code"/>
                        </code>
                        <value>
                            <data>
                                <xsl:value-of select="value/data"/>
                            </data>
                            <code>
                                <xsl:value-of select="value/code"/>
                            </code>
                        </value>
                        <type>
                            <xsl:value-of select="type"/>
                        </type>
                        <data_type>
                            <xsl:value-of select="data_type"/>
                        </data_type>
                    </attribute>
                </xsl:when>
                <xsl:otherwise>
                    <!-- EMPTY -->
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
    </xsl:variable>

    <xsl:variable name="fashion_printing">
        <xsl:for-each select="$varTY/products/product/attributes/attribute">
            <xsl:choose>
                <xsl:when test="contains(., 'Motif_PAP')">
                    <attribute>
                        <code>
                            <xsl:value-of select="code"/>
                        </code>
                        <value>
                            <data>
                                <xsl:value-of select="value/data"/>
                            </data>
                            <code>
                                <xsl:value-of select="value/code"/>
                            </code>
                        </value>
                        <type>
                            <xsl:value-of select="type"/>
                        </type>
                        <data_type>
                            <xsl:value-of select="data_type"/>
                        </data_type>
                    </attribute>
                </xsl:when>
                <xsl:otherwise>
                    <!-- EMPTY -->
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
    </xsl:variable>




    <!-- TOUTE LES VARIABLES DES DIFFERENTS FICHIERS ICI -->
    <xsl:template match="/">
        <!-- Storeland -->
    <xsl:variable name="productCode">
        <xsl:for-each select="products/product/attributes/attribute">
        <xsl:choose>
                <xsl:when test="contains(., 'Product Code')">
                    <attribute>
                        <code><xsl:value-of select="code" /></code>
                        <value>
                            <data><xsl:value-of select="value/data"/></data>
                        </value>
                        <type><xsl:value-of select="type" /></type>
                        <data_type><xsl:value-of select="data_type" /></data_type>
                    </attribute>
                </xsl:when>
                <xsl:otherwise>
                <!-- EMPTY -->
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
    </xsl:variable>

    <xsl:variable name="name">
        <xsl:for-each select="products/product/attributes/attribute">
            <xsl:choose>
                <xsl:when test="contains(., 'Nom')">
                    <attribute>
                        <code>
                            <xsl:value-of select="code"/>
                        </code>
                        <value>
                            <data>
                                <xsl:value-of select="value/data"/>
                            </data>
                        </value>
                        <type>
                            <xsl:value-of select="type"/>
                        </type>
                        <data_type>
                            <xsl:value-of select="data_type"/>
                        </data_type>
                    </attribute>
                </xsl:when>
                <xsl:otherwise>
                <!-- EMPTY -->
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
    </xsl:variable>
    
    <xsl:variable name="title">
        <xsl:for-each select="products/product/attributes/attribute">
            <xsl:choose>
                <xsl:when test="contains(., 'TITLE_ES')">
                    <attribute>
                        <code>
                            <xsl:value-of select="concat(code, ' Mujer Morgan')"/>
                        </code>
                        <value>
                            <data>
                                <xsl:value-of select="value/data"/>
                            </data>
                        </value>
                        <type>
                            <xsl:value-of select="type"/>
                        </type>
                        <data_type>
                            <xsl:value-of select="data_type"/>
                        </data_type>
                    </attribute>
                </xsl:when>
                <xsl:otherwise>
                    <!-- EMPTY -->
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
    </xsl:variable>

    <xsl:variable name="vary_by_color">
        <xsl:for-each select="products/product/articles/article/attributes/attribute">
            <xsl:choose>
                <xsl:when test="contains(., 'ID coloris')">
                    <attribute>
                        <code>
                            <xsl:value-of select="code"/>
                        </code>
                        <value>
                            <data>
                                <xsl:value-of select="value/data"/>
                            </data>
                        </value>
                        <data_type>
                            <xsl:value-of select="data_type"/>
                        </data_type>
                    </attribute>
                </xsl:when>
                <xsl:otherwise>
                    <!-- EMPTY -->
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
    </xsl:variable>

    <xsl:variable name="Taille">
        <xsl:for-each select="products/product/articles/article/attributes/attribute">
            <xsl:choose>
                <xsl:when test="contains(., 'SIZES')">
                    <attribute>
                        <code>
                            <xsl:value-of select="code"/>
                        </code>
                        <value>
                            <data>
                                <xsl:value-of select="value/data"/>
                            </data>
                        </value>
                        <data_type>
                            <xsl:value-of select="data_type"/>
                        </data_type>
                    </attribute>
                </xsl:when>
                <xsl:otherwise>
                    <!-- EMPTY -->
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
    </xsl:variable>

    <xsl:variable name="fashion_main_description_1">
        <xsl:for-each select="products/product/attributes/attribute">
            <xsl:choose>
                <xsl:when test="contains(., 'DESCRIPTION_ES')">
                    <attribute>
                        <code>
                            <xsl:value-of select="code"/>
                        </code>
                        <value>
                            <data>
                                <xsl:value-of select="value/data"/>
                            </data>
                        </value>
                        <data_type>
                            <xsl:value-of select="data_type"/>
                        </data_type>
                    </attribute>
                </xsl:when>
                <xsl:otherwise>
                    <!-- EMPTY -->
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
    </xsl:variable>

    <xsl:variable name="basic_color">
        <xsl:for-each select="products/product/articles/article/attributes/attribute">
            <xsl:choose>
                <xsl:when test="contains(., 'COLOR_CODE')">
                    <attribute>
                        <code>
                            <xsl:value-of select="code"/>
                        </code>
                        <value>
                            <data>
                                <xsl:value-of select="value/data"/>
                            </data>
                            <code>
                                <xsl:value-of select="value/code"/>
                            </code>
                        </value>
                        <type><xsl:value-of select="type"/></type>
                        <data_type>
                            <xsl:value-of select="data_type"/>
                        </data_type>
                    </attribute>
                </xsl:when>
                <xsl:otherwise>
                    <!-- EMPTY -->
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
    </xsl:variable>

    <xsl:variable name="fashion_color">
        <xsl:for-each select="products/product/articles/article/attributes/attribute">
            <xsl:choose>
                <xsl:when test="contains(., 'LIBCOLBUSINESSFR')">
                    <attribute>
                        <code>
                            <xsl:value-of select="code"/>
                        </code>
                        <value>
                            <data>
                                <xsl:value-of select="value/data"/>
                            </data>
                            <code>
                                <xsl:value-of select="value/code"/>
                            </code>
                        </value>
                        <type><xsl:value-of select="type"/></type>
                        <data_type>
                            <xsl:value-of select="data_type"/>
                        </data_type>
                    </attribute>
                </xsl:when>
                <xsl:otherwise>
                    <!-- EMPTY -->
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
    </xsl:variable>
        <products>
            <product>
            <attributes>
                <xsl:copy-of select="$productCode"/>
                <xsl:copy-of select="$name"/>
                <xsl:copy-of select="$constantProductType"/>
                <xsl:copy-of select="$cortrarie"/>
                <xsl:copy-of select="$provider_ref"/>
                <xsl:copy-of select="$title"/>
                <xsl:copy-of select="$brand"/>
                <xsl:copy-of select="$fashion_main_description_1"/>
                <xsl:copy-of select="$fashion_season.year_season"/>
                <xsl:copy-of select="$fashion_clothing_gender_1"/>
                <xsl:copy-of select="$fashion_sleeve"/>
                <xsl:copy-of select="$fashion_cut_1"/>
                <xsl:copy-of select="$length_type"/>
                <xsl:copy-of select="$fashion_printing"/>
                <xsl:copy-of select="$basic_color"/>
            </attributes>
            <articles>
                
            </articles>
            </product>
        </products>
    </xsl:template>
</xsl:stylesheet>